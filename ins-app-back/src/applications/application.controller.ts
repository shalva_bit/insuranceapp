import { Controller, Get, Post, Body, Delete, Param, Patch, ParseIntPipe } from '@nestjs/common';
import { ApplicationService } from './application.service';
import { Application } from './application.entity';
import { InsurnceApplicationDto } from './dto/insurance-application.dto';


@Controller('applications')
export class ApplicationController {
    constructor(private applicationService: ApplicationService){}


    @Get()
    getAllApplications(): Promise<Application[]>{
        return this.applicationService.getAllApplications();
    }

    @Get('/:id')
    getApplicationById(@Param('id',ParseIntPipe) id: number): Promise<Application>{
        return this.applicationService.getApplicationById(id);
    }

    @Post('/apply')
    createApplication(@Body() createApplicationDto: InsurnceApplicationDto): Promise<Application>{
        return this.applicationService.createApplication(createApplicationDto);
    }

    @Delete('/:id')
    deleteApplication(@Param('id',ParseIntPipe) id: number): Promise<Application>{
        return this.applicationService.deleteApplication(id);
    }

    @Post('/update/:id')
    updateCustomerStatus(@Param('id',ParseIntPipe)id: number,@Body()createApplicationDto: InsurnceApplicationDto,
        ): Promise<Application>{
            return this.applicationService.updateApplication(createApplicationDto,id);
        }


}
