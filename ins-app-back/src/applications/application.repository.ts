
import { EntityRepository,Repository } from 'typeorm';
import { Application } from './application.entity';
import { InsurnceApplicationDto } from './dto/insurance-application.dto';
import { ApplicationStatus } from './application-status.enum';
import {getConnection} from "typeorm";



@EntityRepository(Application)
export class ApplicationRepository extends Repository<Application> {
    async createApplication(createApplicationDto: InsurnceApplicationDto): Promise<Application>{
        
        const dto = {...createApplicationDto};

        console.log(dto)

        const apl = new Application();
        apl.first_name = dto.first_name;
        apl.last_name = dto.last_name;
        apl.email_address = dto.email_address;
        apl.phone_number = dto.phone_number;
        apl.company_name = dto.company_name;
        apl.effective_date = new Date(dto.effective_date.replace(/-/g, '\/'));
        apl.status = ApplicationStatus.APPROVED
        apl.primary_al = dto.primary_al;
        apl.primary_el = dto.primary_el;
        apl.primary_gl = dto.primary_gl;

        console.log(apl)

        await apl.save();

        return apl

    }

    async getAllApplications(): Promise<Application[]>{
        const allApps = await this.createQueryBuilder().getMany();
        return allApps;
    }

    async updateApplication(appl: Application): Promise<void>{
        await getConnection()
                .createQueryBuilder()
                .update(Application)
                .set({first_name: "Timber"})
                .where({ id: appl.id })
                .execute();
    }



}