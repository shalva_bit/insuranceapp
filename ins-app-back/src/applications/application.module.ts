import { Module}  from '@nestjs/common';
import { ApplicationService } from './application.service';
import { ApplicationController } from './application.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ApplicationRepository } from './application.repository';

@Module({
  imports: [TypeOrmModule.forFeature([ApplicationRepository])],
  providers: [ApplicationService],
  controllers: [ApplicationController]
})
export class ApplicationModule{

}