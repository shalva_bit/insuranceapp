import { Injectable, NotFoundException } from '@nestjs/common';
import { ApplicationRepository } from './application.repository';
import { InjectRepository } from '@nestjs/typeorm';
import { Application } from './application.entity';
import { InsurnceApplicationDto } from './dto/insurance-application.dto';
import { ApplicationStatus } from './application-status.enum';


@Injectable()
export class ApplicationService{
   
    constructor(
        @InjectRepository(ApplicationRepository)
        private applicationRepository: ApplicationRepository,
    ){}


    async getAllApplications(): Promise<Application[]> {
        const allApps = await this.applicationRepository.getAllApplications();
        return allApps;
    }
    

    async getApplicationById(id: number): Promise<Application> {
        const found = await this.applicationRepository.findOne(id)
        
        if(!found){
            throw new NotFoundException(`user with id: ${id} cant be found`);
        }

        return found;
    }

    async createApplication(createApplicationDto: InsurnceApplicationDto): Promise<Application>{
        return this.applicationRepository.createApplication(createApplicationDto);
    }


    async deleteApplication(id: number): Promise<Application> {
        const apl = await this.getApplicationById(id)
        
        apl.is_deleted=true
        await apl.save();
        return apl;
    }

    async updateApplication(createApplicationDto: InsurnceApplicationDto,id:number): Promise<Application> {
        
        const newData = {...createApplicationDto}
        const appl = await this.applicationRepository.findOne(id)
        
        appl.first_name = newData.first_name;
        appl.last_name = newData.last_name;
        appl.email_address = newData.email_address;
        appl.phone_number = newData.phone_number;
        appl.effective_date = new Date(newData.effective_date.replace(/-/g, '\/'));
        appl.company_name = newData.company_name;
        appl.status = ApplicationStatus.APPROVED;
        appl.primary_al = newData.primary_al;
        appl.primary_gl = newData.primary_gl;
        appl.primary_el = newData.primary_el;

         await appl.save();
        //this.applicationRepository.update(appl.id,appl)
        return appl;
    }
}
