import { BaseEntity,Entity,PrimaryGeneratedColumn,Column } from "typeorm";

@Entity()
export class Application extends BaseEntity{

    @PrimaryGeneratedColumn()
    id: number;

    @Column("varchar", { length: 50 })
    first_name: string;

    @Column("varchar", { length: 50 })
    last_name: string;

    @Column("varchar", { length: 100 })
    email_address: string;

    @Column("varchar", { length: 20 })
    phone_number: string;

    @Column("varchar", { length: 50 })
    company_name: string;

    @Column("varchar", { length: 20 })
    status: string;

    @Column("date")
    effective_date: Date;

    @Column("boolean")
    primary_al: boolean;

    @Column("boolean")
    primary_gl: boolean;
    
    @Column("boolean")
    primary_el: boolean;

    @Column({ type: "boolean", default: false})
    is_deleted: boolean;


}