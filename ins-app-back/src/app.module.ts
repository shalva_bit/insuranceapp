import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';

import { TypeOrmModule } from '@nestjs/typeorm';
import { ormConfig } from './config/orm.config';
import { ApplicationModule } from './applications/application.module';

@Module({
  imports: [
    TypeOrmModule.forRoot(ormConfig),
    ApplicationModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
