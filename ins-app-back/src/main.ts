import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { Logger } from '@nestjs/common';
import * as config from 'config';

async function bootstrap() {
  const logger = new Logger('bootstrap')
  const app = await NestFactory.create(AppModule);
  // console.log(`${process.env.NODE_ENV}`)
  
  const srConfig = config.get("server");
  const frontPort = srConfig.frontPort;
  
  app.enableCors({origin:`http://localhost:${frontPort}`,methods:'GET,POST,DELETE,PATCH'});
  await app.listen(srConfig.port);
}
bootstrap();
