import { TypeOrmModuleOptions } from '@nestjs/typeorm'
import { Application } from '../applications/application.entity'
import * as config from "config";

const db = config.get('db');

export const ormConfig: TypeOrmModuleOptions = {
    type: db.type,
    host: db.host,
    port: db.port,
    username: db.username,
    password: db.password,
    database: db.database,
    // entities: [__dirname+'/../**/*.entity.{js,ts}'],
    entities: [Application],
    synchronize:db.synchronized
}
