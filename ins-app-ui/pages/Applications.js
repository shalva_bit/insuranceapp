import React from 'react';
import ApplicationList from '../Components/ApplicationList';
import fetch from 'node-fetch'
import { useRouter } from 'next/router'
import getConfig from 'next/config';
import LayOut from '../Components/Layout'

function Applications({dt}){
    const router = useRouter()

    function refresh(){
        router.push("/applications")
    }
    return(
        <LayOut>
            <ApplicationList inp={dt} refres = {refresh}></ApplicationList>
        </LayOut>   
    )
}

export async function getServerSideProps() {
    // TODO rm those 2 lines
    const  { publicRuntimeConfig } = getConfig();
    const port = publicRuntimeConfig.backPort;

   const res = await fetch(`http://localhost:${port}/applications/`)
    let dt;
    try {
         dt = await res.json()
    } catch (error) {
        //TODO fix
        console.error(error.message)
    }
  
  return { props: { dt } }
};

export default Applications;