import 'primereact/resources/themes/nova-light/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import 'primeflex/primeflex.css';
import "../styles.css";



export default function MyApp({ Component, pageProps }) {
    return <Component {...pageProps} />
  }