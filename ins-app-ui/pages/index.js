import Head from 'next/head'
import React from 'react';
import TopMenu from '../Components/TopMenu'
import Layout from '../Components/Layout'

 const Home = () => (
        <Layout>
            <h2>Simple insurance app</h2>
        </Layout>
 )

export default Home
