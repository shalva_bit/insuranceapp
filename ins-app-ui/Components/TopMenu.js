import React from 'react';
import {Menubar} from 'primereact/menubar';

export default class TomMenu extends React.Component{
  
     render(){
      const items=[
         {
            label:'Home',
            icon:'pi pi-fw pi pi-fw pi-home',
            url: '/',
         },
         {
            label:'Applications',
            icon:'pi pi-fw pi-file',
            url: '/applications',
         },    
         {
            label:'Apply',
            icon:'pi pi-fw pi-file-o',
            url: '/apply',
         }
      ];

        return(
            <Menubar model={items}/>
        )
     }
}
