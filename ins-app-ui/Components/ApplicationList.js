import Link from 'next/link';
import fetch from 'node-fetch'
import {DataTable} from 'primereact/datatable';
import {Button} from 'primereact/button';
import {Column} from 'primereact/column';
import {Dialog} from 'primereact/dialog';
import React from "react";
import ApplicationForm  from './ApplicationForm';
import getConfig from 'next/config';
import axios from 'axios';




export default class ApplicationList extends React.Component{
    constructor(props){
        super(props);
        this.state={
            aplModal: false,
            title: "",
            rowInfo:{}
        }

        this.actionTemplate = this.actionTemplate.bind(this);
        this.deleteRow = this.deleteRow.bind(this);
        
    }


    deleteRow(rd){
        const idDel = rd.id;
        const  { publicRuntimeConfig } = getConfig();
        const port = publicRuntimeConfig.backPort;
        axios({
            method:'delete',
            url:`http://localhost:${port}/applications/`+idDel,
            config: { 
                headers: 
                        {'Content-Type': 'application/x-www-form-urlencoded',
                }
            }
        })
        .then(resp=>{
            this.props.refres()

        })
        .catch(error=>{
            console.log(error)
        })

    }

    actionTemplate(rowData, column) {
        return <div>
                <Button type="button" onClick={()=>this.deleteRow(rowData)}   icon="pi pi-times-circle" className="p-button-danger" style={{marginRight: '.5em'}}></Button>
                <Button type="button" onClick={()=>this.toggle(rowData)} icon="pi pi-pencil" className="p-button-warning"></Button>
              </div>;
    }

    toggle(rowData) {
        const titleIn = rowData.title;
        
        this.setState({
            aplModal: true,
            rowInfo:rowData
        })
    }

    onHide(name) {
        this.setState({
            [`${name}`]: false
        });
    }

    renderFooter(name) {
        return (
            <div>
                <Button label="Submit" type="submit"  icon="pi pi-check"  className="p-button-secondary"/>
            </div>
        );
    }


    closeAndRefresh = () => {
        this.setState({
            aplModal:false
        })
        this.props.refres();

    }

    render(){
        let cols = [
            {field: 'first_name', header: 'First name',style:{width:'10%'}},
            {field: 'last_name', header: 'Last name',style:{width:'10%'}},
            {field: 'email_address', header: 'Email',style:{width:'18%'}},
            {field: 'phone_number', header: 'Phone',style:{width:'10%'}},
            {field: 'company_name', header: 'Company',style:{width:'20%'}},
            {field: 'effective_date', header: 'Date',style:{width:'8%'}},
            {field: 'status', header: 'Status',style:{width:'8%'}},
            {field: 'primary_al', header: 'Auto liab',style:{width:'6%'}},
            {field: 'primary_gl', header: 'General liab',style:{width:'6%'}},
            {field: 'primary_el', header: 'Employee liab',style:{width:'6%'}}
        ];

        let dynamicColumns = cols.map((col,i) => {
            return <Column key={i} field={col.field} header={col.header} style={col.style} />;
        });
        
        const filteredApplForBoolean = this.props.inp.map(app => ({
            id:app.id,
            first_name: app.first_name,
            last_name: app.last_name,
            email_address: app.email_address,
            phone_number: app.phone_number,
            company_name: app.company_name,
            effective_date: app.effective_date,
            status: app.status,
            primary_al: (app.primary_al)==true?"YES":"NO",
            primary_gl: (app.primary_gl)==true?"YES":"NO",
            primary_el: (app.primary_el)==true?"YES":"NO",
            is_deleted:app.is_deleted
        })).filter(apl => apl.is_deleted!==true);

        return (
            <div>
                <div className="content-section introduction">
                    <div className="feature-intro">
                        <h1>All applications</h1>
                    </div>
                    <Dialog visible={this.state.aplModal}  style={{width:"80%"}} header="Application Details" modal={true} onHide={() => this.onHide('aplModal')}>
                        <ApplicationForm upd={this.state.rowInfo} rfrsh={this.closeAndRefresh}></ApplicationForm>
                    </Dialog>
                </div>
                <div className="content-section implementation">
                    <DataTable value={filteredApplForBoolean} paginator={true} rows={15} resizableColumns={true} columnResizeMode="fit" >
                        {dynamicColumns}
                        <Column body={this.actionTemplate} header="Delete/Edit" style={{textAlign:'center', width: '8em'}} />
                    </DataTable>
                </div>
            </div>
        )
    }
}


// export async function getServerSideProps() {
//       // const res = await fetch('https://jsonplaceholder.typicode.com/todos')
//      const res = await fetch('http://localhost:3020/applications/')
    
//     const dt = await res.json()
//     console.log(dt)
//     // dt.map(item => console.log(item))
//     return { props: { dt } }
//   };