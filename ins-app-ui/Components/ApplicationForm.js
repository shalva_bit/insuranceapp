import React from 'react';
import {Card} from 'primereact/card';
import {InputText} from 'primereact/inputtext';
import {InputMask} from 'primereact/inputmask';
import {Checkbox} from 'primereact/checkbox';
import {Button} from 'primereact/button';
import {Messages} from 'primereact/messages';
import getConfig from 'next/config';
import axios from 'axios';


export default class ApplicationForm extends React.Component{
    constructor(props){
        super(props);
        this.state={
            id:"",
            first_name:"",
            last_name:"",
            email_address:"",
            phone_number:"",
            company_name:"",
            status:"PENDING",
            effective_date:"",
            primary_al:false,
            primary_gl:false,
            primary_el:false

        }
        this.showSuccess = this.showSuccess.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }


    componentDidMount(){
        if(this.props.upd){
            const toUpd = this.props.upd;
            this.setState({
                id:toUpd.id,
                first_name:toUpd.first_name,
                last_name:toUpd.last_name,
                email_address:toUpd.email_address,
                phone_number:toUpd.phone_number,
                company_name:toUpd.company_name,
                status:"PENDING",
                effective_date:toUpd.effective_date,
                primary_al:toUpd.primary_al==="YES"?true:false,
                primary_gl:toUpd.primary_gl==="YES"?true:false,
                primary_el:toUpd.primary_el==="YES"?true:false,
            })
        }
    }
    showSuccess() {
        this.messages.show({severity: 'success', summary: 'Success Message', detail: 'Application submitted'});
    }

    clearForm(){
        this.setState({
            id:"",
            first_name:"",
            last_name:"",
            email_address:"",
            phone_number:" ",
            company_name:"",
            status:"PENDING",
            effective_date:"",
            primary_al:false,
            primary_gl:false,
            primary_el:false,
        })
    }

    handleSubmit(event){
        event.preventDefault();

        const application = {
            first_name:this.state.first_name,
            last_name:this.state.last_name,
            email_address:this.state.email_address,
            phone_number:this.state.phone_number,
            company_name:this.state.company_name,
            status:this.state.status,
            effective_date:this.state.effective_date,
            primary_al:this.state.primary_al,
            primary_gl:this.state.primary_gl,
            primary_el:this.state.primary_el,
        }

        var zis = this;
        const  { publicRuntimeConfig } = getConfig();
        const port = publicRuntimeConfig.backPort;
        axios({
            // `${this.props.upd?"update":"post"}`,
            method:"post",
            url:`http://localhost:${port}/applications/${this.props.upd?`update/${this.props.upd.id}`:"apply"}/`,
            data:application,

            config: { headers: {'Content-Type': 'application/x-www-form-urlencoded',
            'Access-Control-Allow-Origin': '*' }}
        })
        .then(resp=>{
            zis.clearForm();
            if(zis.props.rfrsh){
                zis.props.rfrsh();
            }
            this.showSuccess();
        })
        .catch(error=>{
            console.log(error)
        })
    }

      handleInputChange(event) {

        const target = event.target;
        const value = target.name === 'primary_al' || target.name === 'primary_gl' || target.name === 'primary_el' ? target.checked : target.value;
        const name = target.name;

            this.setState({
                [name]: value
            });
      }

    render(){
        return (
            

            <div className="p-grid p-fluid p-justify-center">
                <div className="p-col-6">
                    <Messages ref={(el) => this.messages = el} />
                    <h2 style={{textAlign:'center'}}>{this.props.upd?"Update ":"Application "}Form</h2>
                    <Card>
                        <form  onSubmit={this.handleSubmit}>
                            <div className="p-grid p-fluid p-justify-center">
                                <div className="p-col-8">
                                    <div className="p-grid p-fluid p-justify-center">
                                        <div className="p-col-6">
                                            <label htmlFor="first_n" style={{marginBottom: '.5rem',display:"inline-block"}}>First name</label>
                                            <InputText id="first_n"  type="text" name="first_name" value={this.state.first_name} onChange={this.handleInputChange} />   
                                        </div>
                                        <div className="p-col-6">
                                            <label htmlFor="last_n" style={{marginBottom: '.5rem',display:"inline-block"}}>Last name</label>
                                            <InputText id="last_n" type="text" name="last_name" value={this.state.last_name} onChange={this.handleInputChange} />
                                        </div>
                                    </div>
                                </div>
                                <div className="p-col-8">
                                    <label htmlFor="email" style={{marginBottom: '.5rem',display:"inline-block"}}>Email</label>
                                    <InputText id="email" type="email" name="email_address" value={this.state.email_address} onChange={this.handleInputChange} />
                                </div>
                                <div className="p-col-8">
                                <label htmlFor="phone" style={{marginBottom: '.5rem',display:"inline-block"}}>Phone</label>
                                    <InputMask id="phone" mask='(999) 999-9999' name="phone_number" type="tel" value={this.state.phone_number}
                                    onChange={this.handleInputChange}
                                    />
                                </div>
                                <div className="p-col-8">
                                    <div className="p-grid p-fluid">
                                        <div className="p-col-6">
                                            <label htmlFor="company" style={{marginBottom: '.5rem',display:"inline-block"}}>Company name</label>
                                                <InputText id="company" type="text" name="company_name" value={this.state.company_name} onChange={this.handleInputChange} />
                                        </div>
                                        <div className="p-col-6">
                                            <label htmlFor="dt" style={{marginBottom: '.5rem',display:"inline-block"}}>Effective date</label>
                                            <div>
                                            <input type="date" name="effective_date" value={this.state.effective_date} className="p-inputtext p-component dblock" onChange={this.handleInputChange} id="birthday"/>
                                            </div> 
                                        </div>
                                    </div>
                                </div>
                                <div className="p-col-8">
                                <div className="p-grid p-fluid p-justify-center">
                                    <div className="p-col-12">
                                        <Checkbox inputId="cb1" name="primary_al" onChange={this.handleInputChange} checked={this.state.primary_al}></Checkbox>
                                        <label htmlFor="cb1" className="p-checkbox-label">Primary AL(Auto Liability)</label>
                                    </div>
                                    <div className="p-col-12">
                                        <Checkbox inputId="cb2" name="primary_gl" onChange={this.handleInputChange} checked={this.state.primary_gl}></Checkbox>
                                        <label htmlFor="cb2" className="p-checkbox-label">Primary GL(General Liability)</label>
                                    </div>
                                    <div className="p-col-12">
                                        <Checkbox inputId="cb3" name="primary_el" onChange={this.handleInputChange} checked={this.state.primary_el}></Checkbox>
                                        <label htmlFor="cb3" className="p-checkbox-label">Primary EL(Employ Liability)</label>
                                    </div>
                                </div>
                                </div>
                                <div className="p-col-8">
                                <div className="p-grid p-fluid" style={{textAlign:"right"}}>
                                    <div className="p-col-4 p-offset-8">
                                        <Button label="Submit" type="submit" className="p-button-raised p-button-rounded" />
                                    </div>
                                </div>
                                </div> 
                            </div>
                        </form>
                    </Card>
                </div>
            </div>   
        )
    }
}