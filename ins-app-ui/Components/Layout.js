import React from 'react'
import TopMenu from './TopMenu'


export default class Layout extends React.Component{
   
    render(){
        return(
            <div>
                <TopMenu></TopMenu>
                {this.props.children}
            </div>
        )
    }
}